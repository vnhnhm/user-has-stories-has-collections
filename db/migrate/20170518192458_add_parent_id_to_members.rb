class AddParentIdToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :parent_id, :integer
  end
end
