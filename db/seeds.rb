# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Family.delete_all
10.times do |index|
	Family.create(
		title: "family-#{index}"
	)
end

Member.delete_all
50.times do |index|
	Member.create(
		first_name: "name-#{index}",
		family_id: rand(1..10)
	)
end
